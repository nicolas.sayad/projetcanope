<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableau de bord</title>
    <!-- Inclure les styles Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Inclure les styles Font Awesome pour les icônes -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <!-- Styles personnalisés -->
    <style>
        body {
            background-color: #f8f9fa;
            font-family: Arial, sans-serif;
        }
        .container {
            padding-top: 50px;
        }
        .card-header {
            background-color: #fff;
            color: #fff;
            border-radius: 10px 10px 0 0;
        }
        .card-body {
            background-color: #fff;
            border-radius: 0 0 10px 10px;
            box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.1);
        }
        .card-title {
            color: #007bff;
            font-weight: bold;
        }
        .card-body .card {
            height: 200px;
            border-radius: 10px;
            border: 2px solid #007bff;
            transition: all 0.3s ease;
        }
        .card-body .card:hover {
            border-color: #0056b3;
        }
        .card-body .card i {
            font-size: 3rem;
            margin-top: 20px;
            transition: all 0.3s ease;
        }
        .card-body .card:hover i {
            font-size: 4rem;
        }
        .card-body .card p {
            font-size: 1.2rem;
            margin-top: 15px;
            transition: all 0.3s ease;
        }
        .card-body .card:hover p {
            font-size: 1.5rem;
        }
        .btn-primary {
            background-color: #007bff;
            border-color: #007bff;
        }
        .btn-primary:hover {
            background-color: #0056b3;
            border-color: #0056b3;
        }
        .btn-danger {
            background-color: #dc3545;
            border-color: #dc3545;
        }
        .btn-danger:hover {
            background-color: #c82333;
            border-color: #c82333;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tableau de bord</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <i class="fas fa-user"></i>
                                        <p class="mt-2">Expositions</p>
                                        <a href="expositions.php" class="btn btn-primary">Expositions</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <i class="fas fa-calendar-alt"></i>
                                        <p class="mt-2">Calendrier</p>
                                        <a href="planning.php" class="btn btn-primary">Calendrier</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <i class="fas fa-cog"></i>
                                        <p class="mt-2">Paramètres</p>
                                        <a href="parametres.php" class="btn btn-primary">Paramètres</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="logout.php" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> Se déconnecter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inclure les scripts Bootstrap et jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
