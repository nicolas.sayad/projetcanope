<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Planning</title>
    <!-- Inclure les styles Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Inclure les styles personnalisés -->
    <style>
        /* Ajoutez ici vos styles personnalisés pour la page planning */
        body {
            background-color: #f8f9fa;
        }
        .container {
            padding-top: 80px;
        }
        .card {
            border: none;
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
        }
        .card-header {
            background-color: #007bff;
            color: #fff;
        }
        .card-title {
            color: #007bff;
            font-weight: bold;
        }
        #planning {
            background-color: #ffff;
            border-radius: 8px;
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
        }
        .btn-back {
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Planning</h3>
                    </div>
                    <div class="card-body">
                        <!-- Contenu de votre planning ici -->
                        <div id="planning"></div>
                        <!-- Bouton de retour vers le dashboard -->
                        <a href="Dashboard.php" class="btn btn-secondary btn-back">Retour au Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inclure les scripts Bootstrap et Day.js -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/dayjs@1.10.7/dayjs.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Fonction pour formater la date
            function formatDate(date) {
                return dayjs(date).format('YYYY-MM-DD');
            }

            // Générer les événements pour le planning
            function generateEvents() {
                // Vous pouvez remplir cette fonction avec les événements de votre planning
                return [
                    {
                        title: 'Réunion',
                        start: formatDate('2022-02-14T10:00:00'),
                        end: formatDate('2022-02-14T12:00:00')
                    },
                    {
                        title: 'Formation',
                        start: formatDate('2022-02-16T14:00:00'),
                        end: formatDate('2022-02-16T16:00:00')
                    }
                ];
            }

            // Afficher les événements sur le planning
            function displayEvents() {
                var planningDiv = document.getElementById('planning');
                var events = generateEvents();
                events.forEach(function(event) {
                    var eventElement = document.createElement('div');
                    eventElement.textContent = event.title + ' : ' + event.start + ' - ' + event.end;
                    planningDiv.appendChild(eventElement);
                });
            }

            // Appel de la fonction d'affichage des événements
            displayEvents();
        });
    </script>
</body>
</html>
