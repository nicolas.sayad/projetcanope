<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des expositions</title>
    <!-- Inclure les styles Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Styles personnalisés -->
    <style>
        /* Ajoutez ici vos styles personnalisés pour la page des expositions */
        body {
            background-color: #f8f9fa;
        }
        .container {
            padding-top: 80px;
        }
        .exposition {
            margin-bottom: 30px;
        }
        .exposition img {
            max-width: 100%;
            height: auto;
        }
        .exposition-title {
            font-weight: bold;
            margin-top: 10px;
        }
        .exposition-description {
            margin-top: 5px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Liste des expositions</h1>
        <div class="exposition">
            <img src="image1.jpg" alt="Exposition 1">
            <h2 class="exposition-title">Exposition 1</h2>
            <p class="exposition-description">Description de l'exposition 1...</p>
            <a href="exposition1.php" class="btn btn-primary">Plus d'informations</a>
        </div>
        <div class="exposition">
            <img src="image2.jpg" alt="Exposition 2">
            <h2 class="exposition-title">Exposition 2</h2>
            <p class="exposition-description">Description de l'exposition 2...</p>
            <a href="exposition2.php" class="btn btn-primary">Plus d'informations</a>
        </div>
        <!-- Ajoutez autant de sections d'expositions que nécessaire -->
    </div>

    <!-- Inclure les scripts Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
