<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paramètres</title>
    <!-- Inclure les styles Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Styles personnalisés -->
    <style>
        /* Ajoutez ici vos styles personnalisés pour la page paramètres */
        body {
            background-color: #f8f9fa;
        }
        .container {
            padding-top: 80px;
        }
        .card {
            border: none;
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
        }
        .card-header {
            background-color: #007bff;
            color: #fff;
        }
        .card-title {
            color: #007bff;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Paramètres</h3>
                    </div>
                    <div class="card-body">
                        <!-- Contenu de vos paramètres ici -->
                        <form>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Email</label>
                                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Notifications</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option>Activer</option>
                                    <option>Désactiver</option>
                                </select>
                            </div>
                            <!-- Bouton de retour vers le dashboard -->
                            <a href="Dashboard.php" class="btn btn-secondary">Retour au Dashboard</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inclure les scripts Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
