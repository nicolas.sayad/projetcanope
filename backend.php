<?php
// Vérifier si des données ont été envoyées via le formulaire
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Vérifier si tous les champs requis sont remplis
    if (isset($_POST['email']) && isset($_POST['password'])) {
        // Récupérer les données du formulaire
        $email = $_POST['email'];
        $password = $_POST['password'];

        // Paramètres de connexion à la base de données
        $host = 'localhost'; // Adresse de la base de données
        $username = 'user'; // Nom d'utilisateur MySQL
        $password_db = 'rootMysql'; // Mot de passe MySQL
        $database = 'pjcanope'; // Nom de la base de données

        // Connexion à la base de données
        $conn = new mysqli($host, $username, $password_db, $database);

        // Vérifier la connexion
        if ($conn->connect_error) {
            die("La connexion à la base de données a échoué : " . $conn->connect_error);
        }

        // Requête pour vérifier si l'utilisateur existe et si le mot de passe est correct
        $sql = "SELECT * FROM utilisateurs WHERE email='$email' AND password='$password'";
        $result = $conn->query($sql);

        // Vérifier si l'utilisateur a été trouvé
        if ($result->num_rows > 0) {
            // Rediriger l'utilisateur vers la page Dashboard.php en cas de succès
            header("Location: dashboard.php");
            exit;
        } else {
            // Rediriger l'utilisateur vers la page de connexion avec un message d'erreur
            header("Location: index.html?error=1");
            exit;
        }

        // Fermer la connexion à la base de données
        $conn->close();
    } else {
        // Rediriger l'utilisateur vers la page de connexion avec un message d'erreur si les données sont manquantes
        header("Location: index.html?error=1");
        exit;
    }
}
?>
